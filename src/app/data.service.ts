import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  users = [
    {id: 1, username: "User01", password: "Test@123", email: "user@aemenersol.com"},
    {id: 2, username: "User02", password: "pwd02", email: "c002@email.com"}
  ];

  constructor() { }

  public getContacts():Array<{id, username, password, email}>{
    return this.users;
  }
  public createContact(user: {id, username, password, email}){
    this.users.push(user);
  }
}
