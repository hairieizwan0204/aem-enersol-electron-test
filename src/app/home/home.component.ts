import { Component } from '@angular/core';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';

import { AuthenticationService } from '@app/_services';
import { User } from '@app/_models';
import { UserService } from '@app/_services';

@Component({ templateUrl: 'home.component.html' })
export class HomeComponent {
    loading = false;
    users: User[];
    chartOptions = {
        responsive: true
      };
    chartData = [
        { data: [330, 600, 260, 700], label: 'Account A' },
        { data: [120, 455, 100, 340], label: 'Account B' },
        { data: [45, 67, 800, 500], label: 'Account C' }
      ];
    chartLabels = ['January', 'February', 'Mars', 'April'];
    donutData = [
        { data: [45, 25, 30], label: 'Dummy Data' }
    ];
    donutLabels = ['Red','Blue','Yellow'];

    constructor(
        private userService: UserService,
        private router: Router, 
        private authenticationService: AuthenticationService
        ){ }

        logout() {
            this.authenticationService.logout();
            this.router.navigate(['/login']);
        }

    ngOnInit() {
        this.loading = true;
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.loading = false;
            this.users = users;
        });
    }
}